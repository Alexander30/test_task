<?php

if (isset($_GET['delete'])) {

    $result = $db->deleteByIdQuery('purchases', $_GET['delete']);
    if ($result) {
        echo json_encode(['result' => 'success']);
    } else {
        echo json_encode(['result' => 'false']);
    }
}

if (isset($_GET['add'])) {
    $result = $db->insertQuery('purchases', [
        'name' => $_GET['name'],
        'category' => $_GET['category'],
        'weight' => $_GET['weight']
            ]
    );
    if ($result) {
        $category = $db->buildSelectQuery('category', ['id', 'category'], [['id', '=', $_GET['category']]
        ]);
        $category = $category->fetch_assoc();
        echo json_encode(['result' => 'success',
            'id' => $result,
            'category_id' => $category['id'],
            'category' => $category['category'],
            'weight' => $_GET['weight'],
            'name' => $_GET['name']
        ]);
    } else {
        echo json_encode(['result' => 'false']);
    }
}

if (isset($_GET['edit'])) {
    $result = $db->buildUpdateByIdQuery('purchases', [
        'name' => $_GET['name'],
        'category' => $_GET['category'],
        'weight' => $_GET['weight']
            ], $_GET['id']);
    
    if ($result) {
        echo json_encode(['result' => 'success']);
    } else {
        echo json_encode(['result' => 'false']);
    }
}

$has_view = false;

