<?php 

$result = $db->buildSelectQuery('purchases');

$result1 = [];
foreach ($result as $value) {
    
    $category = $db->buildSelectQuery('category',
            ['category'],
            [['id', '=', $value['category']]
            
            ]);
    $category = $category->fetch_assoc();
    $row[] = ['id' => $value['id'],
        'name' => $value['name'],
        'category' => $category['category'],
        'category_id' => $value['category'],
        'weight' => $value['weight']];
}

$all_categories = $db->buildSelectQuery('category');
$has_view = true;
?>
