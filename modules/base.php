<?php

class base {
    
    
    private $path;
    
    
    public function setPath($path) {
        $this->path = (string) $path;
    }
    /** so easy, we don`t need new tampleter,
     * just to separate html and php
     * **/
    public function parseView() {
        include_once 'views/'.$this->path.'php';  
    }
    
}

$view = new base($path);

