<?php

error_reporting('E_ALL|E_STRICT');
$path = $_GET;

foreach ($path as $value) {

    if (!preg_match('/^[a-zA-Z0-9_+|\s]{1,}$/', $value))
        die('Hacking attack, die!');
}

if (!isset($path['module'])) {
    die('There is no module');
}

require ('config/db_config.php');
require ('classes/mysql.class.php');

$module = $path['module'];
{
    include_once 'modules/' . $module . ".php";
    if ($has_view) {
        include_once 'views/header.php';
        include_once 'views/' . $module . ".php";
        include_once 'views/footer.php';
    }
}






