$(function () {
    /**slider **/
    $("#slider-range").slider({
        range: true,
        min: 1700,
        max: 61300,
        values: [1700, 61300],
        slide: function (event, ui) {
            $("input[name=begin-interval]").val(ui.values[0]);
            $("input[name=end-interval]").val(ui.values[1]);
        }
    });

    $("input[name=begin-interval]").val($("#slider-range").slider("values", 0));
    $("input[name=end-interval]").val($("#slider-range").slider("values", 1));

    /**droplist**/
    $(".filter-select").selectmenu();

});

$(document).ready(function () {

    $('table').on('click', '.delete-button', function () {
        var tr = $(this).parents('tr');
        var id = $(this).parents('td').find('.hidden').val();
        $.get('/', 'delete=' + id + '&module=edittable', function (data) {
            var result = jQuery.parseJSON(data);
            if (result.result == 'success') {
                $(tr).fadeOut(2000, function () {
                    $(this).remove();
                });

            } else {
                alert('Произошла ужасная внутренняя ошибка');
            }
        });
    });




    $('table').on('click', '.edit-button', function () {
        var tr = $(this).parents('tr');

        var row = $(tr).find('.id').html();
        var name = $(tr).find('.name').html();
        var category = $(tr).find('.category').html();
        var category_id = $(tr).find('.category').attr('category-id');
        var weight = $(tr).find('.weight').html();



        var popup = $('.popup').clone();

        $(popup).find('input[name=name]').val(name);

        var option = "<option value = '" + category_id + "'>" + category + "</option>";
        $(popup).find('select[name=category]').prepend(option);
        $(popup).find('input[name=name]').val(name);
        $(popup).find('input[name=weight]').val(weight);
        $(popup).find('.add').val('Edit');


        $('body').append(popup);
        $(popup).show();

        $(popup).on('click', '.add', function () {
            var form = $(this).closest('form');
            var sendData = $(this).closest('form').serialize();
            sendData += "&module=edittable&edit=1&id=" + row;
           
            $.get('/', sendData, function (data) {
                
                var result = jQuery.parseJSON(data);
                if (result.result == 'success') {
                    
                    var sended = $(form).serializeArray();
                    console.log(sended);
                    $(tr).find('.name').html($(popup).find('input[name=name]').val());
                    $(tr).find('.category').html($(popup).find('option:selected').html());
                    $(tr).find('.category').attr('category-id', $(popup).find('select').val());
                    $(tr).find('.weight').html($(popup).find('input[name=weight]').val());
                    $(popup).remove();

                } else {
                    alert('Произошла ужасная внутренняя ошибка');
                }
            });
        });






    });

    $('.add-button').on('click', function () {
        $('.popup').show();
    });

    $('body').on('click', '.close', function () {
        $('.popup').hide();
    });

    $('.add').on('click', function () {
        var sendData = $(this).closest('#add-form').serialize();
        sendData += "&module=edittable&add=1";
        $.get('/', sendData, function (data) {
            var result = jQuery.parseJSON(data);
            if (result.result == 'success') {
                $('.popup').hide();
                var insertTr = "<tr>\n\
                                <td class ='id'>" + result.id + "</td>\n\
\n\                             <td class = 'name'>" + result.name + "</td>\n\
                                <td class = 'category' category-id = " + result.category_id + ">" + result.category + "</td>\n\
                                <td class='weight'>" + result.weight + "</td>\n\
                             <td>\n\
                                <input class ='edit-button' type ='button' id ='" + result.id + "' value ='Редактировать'/>\n\
                                <input class ='delete-button' type ='button' id ='" + result.id + "' value ='Удалить'" + result.id + "'/>\n\
\n\                            <input class ='hidden' type ='hidden' name = 'delete' value ='" + result.id + "' />\n\
                            </tr>";

                $('.purchases tbody').append(insertTr);

            } else {
                alert('Произошла ужасная внутренняя ошибка');
            }
        });
    });



});

