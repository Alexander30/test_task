<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Front-end</title>
        <script src="/public/js/libraries/jquery-2.2.3.min.js"></script>
        <script src="/public/js/site.js"></script>
        <link rel="stylesheet" href="/public/css/style.css">
        <link rel="stylesheet" href="/public/js/libraries/jquery-ui-1.11.4.custom/jquery-ui.theme.css">
        <link rel="stylesheet" href="/public/js/libraries/jquery-ui-1.11.4.custom/jquery-ui.structure.css">
        <script src="/public/js/libraries/jquery-ui-1.11.4.custom/jquery-ui.js"></script>

    </head>
    <body>

