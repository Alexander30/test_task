<?php
class mysql {

    protected $db_config;
    protected $connection;
    protected $query;
    protected $result;

    function __construct($db_config) {
        $this->db_config = $db_config;
        $this->connection = new mysqli(
                $this->db_config['host'], $this->db_config['username'], $this->db_config['password'], $this->db_config['db_name']
        );




        if (mysqli_connect_errno() != 0) {
            die(mysqli_connect_error());
        }

        mysqli_set_charset($this->connection, $this->db_config['charset']) or
                die(mysqli_error($this->connection));


        return $this;
    }

    function __destruct() {
        $this->connection->close();
    }

    public function getResult() {
        return $this->result;
    }

    protected function clearInput() {
        if (isset($this->query)) {
            $this->query = mysql_escape_string(htmlspecialchars(trim($this->query)));
        }
    }
    /**
     * build select query
     * 
     * @param string $tableName
     * @param array $fields
     * @param array $conditions
     * @return \mysql
     */
    public function buildSelectQuery($tableName, $fields, $conditions) {
        $query = "SELECT ";
        if (isset($fields)) {

            for ($i = 0; $i < count($fields); $i++) {
                $query.= " `" . $tableName . "`.`" . $fields[$i] . "`";
                if ($i != count($fields) - 1) {
                    $query.= ",";
                }
            }
        } else {
            $query.= ' * ';
        }
        $query.= " FROM `" . $tableName . "` ";

        if (isset($conditions)) {
            $query.= "WHERE ";
            $j = 0; 
            foreach ($conditions as $value) {
                $j++;
                $query.= " ".$value[0]." ".$value[1]." ".$value[2];
                if ($j != count($conditions)) {
                    $query.= ",";
                }
            }
        } 

       
        $this->query = $query;
        $this->clearInput();
        $result = $this->connection->query($query);
        return $result;
        
        
    }
    /**
     * build delete Query
     * 
     * @param sting $tableName
     * @param id $id
     * @return \mysql
     */
    public function deleteByIdQuery($tableName, $id) {
        $query = "DELETE FROM `".$tableName."` WHERE id = ".$id;
        $this->query = $query;
        $this->clearInput();
        
        $result = $this->connection->query($query);
        return $result;
    }
    
    /**
     * build insert Query
     * 
     * @param string $tableName
     * @param array $insert_values
     * @return \mysql
     */
    public function insertQuery($tableName, $insert_values) {
        $query = "INSERT INTO `".$tableName."` (";
        
        $keys = "";
        $values = "";
        $j = 0;
        foreach ($insert_values as $key => $value ) {
            $j++;
            $keys.= $key;
            $values.=$value;
            if ($j != count($insert_values)) {
                    $keys.= ",";
                    $values.= ", ";
                }
        }
        
        $query.= $keys.") VALUES (".$values.")";
        $this->query = $query;
        $this->clearInput();
        
        $result = $this->connection->query($query);
        return $this->connection->insert_id;
    }
    
    public function buildUpdateByIdQuery($tableName, $update_values, $id) {
        $query = "UPDATE ".$tableName." SET ";
        foreach ($update_values as $key => $value) {
            $j++;
            $query.= $key." = '".$value."'";
            if ($j != count($update_values)) {
                    $query.= ", ";
                }
        }
        
        $query.= " WHERE id =".$id;
        
        $this->query = $query;
        $this->clearInput();
        
        $result = $this->connection->query($query);
        return $result;
    }
}

$db = new mysql($db_config);



